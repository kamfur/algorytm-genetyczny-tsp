﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace algorytmGenetyczny
{
    class Ocena : IEquatable<Ocena>, IComparable<Ocena>
    {
        public double wartosc { get; set; }
        public int ID { get; set; }



        public bool Equals(Ocena other)
        {
            if (other == null) return false;
            return (this.wartosc.Equals(other.wartosc));
        }

        public int CompareTo(Ocena other)
        {
            if (other == null)
                return 1;

            else
                return this.wartosc.CompareTo(other.wartosc);
        }
    }
}
