﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace algorytmGenetyczny
{
    class Program
    {
        static int liczbaOsobnikow=20;
        static int max=0;
        static int MinWay=11111111;
        static int bestit;
        
        
        static void Main(string[] args)
        {
             
            int[,] Pop;
            int[,] PopTmp;
            int[,] Distance;
            int[] Rodzice;
            List<Ocena> Ocena = new List<Ocena>();
            string[] lines = File.ReadAllLines(@"C:\Users\Kamil\Documents\Studia UŚ\Sztuczna inteligencja w eksploracji danych\algorytmGenetyczny\berlin52.txt");
            Distance = new int[int.Parse(lines[0]), int.Parse(lines[0])];
            Pop = new int[liczbaOsobnikow, int.Parse(lines[0])];
            PopTmp = new int[liczbaOsobnikow, int.Parse(lines[0])];
            Rodzice = new int[liczbaOsobnikow];
            

            readFile(Distance, lines);
            generujPopulacjePoczatkowa(Pop);
            
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            
            while (stopwatch.Elapsed<TimeSpan.FromSeconds(20))
            {
                funkcja_oceny(Pop,Distance, Ocena);
                selekcja(Pop, PopTmp, Rodzice, Ocena);
                krzyzowanie(Pop, PopTmp);
                mutacja(Pop);
            }
            Console.WriteLine("Najmniejsza" + MinWay);

                Console.ReadKey();
        }

        static void readFile(int[,] tab, string[] lines)
        {
            

            for (int i = 1; i < lines.Length; i++)
            {
                string[] line = lines[i].Split(' ');
                for (int j = 0; j < line.Length-1; j++)
                {
                    tab[i-1, j] = int.Parse(line[j]);
                    tab[j, i-1] = int.Parse(line[j]);
                }
            }

            
        }
        private static void generujPopulacjePoczatkowa(int[,] Pop)
        {
            
            Random r = new Random();
            for (int i = 0; i < Pop.GetLength(0); i++)
            {
                ArrayList l = new ArrayList();
                for (int m = 0; m < Pop.GetLength(1); m++) l.Add(m);
               
                int max = l.Count;
                for (int j = 0; j < Pop.GetLength(1); j++)
                {
                    int re = r.Next(0, max);
                    Pop[i, j] = (int)l[re];
                    l.RemoveAt(re);
                    max--;
                }
            }
        }

        static void funkcja_oceny(int[,]Pop, int[,] Distance, List<Ocena> Ocena)
        {

            
            for (int i = 0; i < Pop.GetLength(0); i++)
            {
                int suma = 0;
                max = 0;
                for (int j = 0; j < Pop.GetLength(1); j++)
                {
                    if(j < Pop.GetLength(1)-1 ) suma =suma +Distance[Pop[i, j], Pop[i, j + 1]];
                        else suma =suma+Distance[Pop[i, j], Pop[i, 0]];
                }
                Ocena.Add(new Ocena() { wartosc = suma, ID = i });

                //Console.WriteLine(suma);
                if (suma > max) max = suma;
                if (suma < MinWay)
                {
                    
                    MinWay = suma;
                    //Console.WriteLine("Najmniejsza"+MinWay);
                    bestit = i;
                }
            }
            Ocena.Sort();
           
        }



        static void selekcja(int[,] Pop, int[,] PopTmp,  int[] Rodzice, List<Ocena> Ocena)
        {
         
           
            List<int> skumulowana = new List<int>();
            int current = 0;
           


            var rouleteWheel = new List<Ocena>();
            var accumulativePercent = 0.0;
           
            double max = Ocena.Sum(s => s.wartosc);
           
            foreach (Ocena o in Ocena)
            {
                accumulativePercent= accumulativePercent+o.wartosc;
                double sumarum = accumulativePercent / max;
                rouleteWheel.Add(new Ocena() { wartosc = sumarum, ID = o.ID });
               
            }
            int chromosomeIndex = 0;
            Random r = new Random();
            for (int i = 0; i < Rodzice.Length; i++)
            {
                var pointer = r.NextDouble();
                chromosomeIndex = rouleteWheel.FirstOrDefault(ro => ro.wartosc >= pointer).ID;
                Rodzice[i] = chromosomeIndex;
            }
            Ocena.Clear();
            rouleteWheel.Clear();
            
            for (int i = 0; i < PopTmp.GetLength(0); i++)
            {
                int sel = Rodzice[i];
                for (int j = 0; j < PopTmp.GetLength(1); j++)
                {

                    PopTmp[i, j] = Pop[sel, j];
                }
               

            }

        }
      
       

        static void krzyzowanie(int[,] Pop, int[,]PopTmp)
        {
            
            Random r = new Random(34);
            int split1 = r.Next(0, Pop.GetLength(1)/2);
            int split2 = r.Next(split1+1, Pop.GetLength(1) - 1);
            Random rp = new Random();
            for (int i = 0; i < PopTmp.GetLength(0)-1; i++)
            {
               
                
                if (rp.Next(1,100) < 85)
                {
                    for (int j = 0; j < PopTmp.GetLength(1); j++)
                    {
                        List<int> istniejaceChromA = new List<int>();
                        List<int> istniejaceChromB = new List<int>();
                        if (j > split1 && j < split2)
                        {

                            {
                                Pop[i, j] = PopTmp[i, j];
                                Pop[i + 1, j] = PopTmp[i + 1, j];
                                istniejaceChromA.Add(Pop[i, j]);
                                istniejaceChromB.Add(Pop[i + 1, j]);
                            }

                        }
                        
                        else
                        if (istniejaceChromA.Exists(x => x == PopTmp[i + 1, j]) && istniejaceChromB.Exists(y => y == PopTmp[i, j]))
                        {
                            Pop[i, j] = PopTmp[i + 1, j];
                            Pop[i + 1, j] = PopTmp[i, j];
                            istniejaceChromA.Add(Pop[i, j]);
                            istniejaceChromB.Add(Pop[i + 1, j]);
                        }
                        else
                        {
                            Pop[i, j] = PopTmp[i, j];
                            Pop[i + 1, j] = PopTmp[i + 1, j];
                            istniejaceChromA.Add(Pop[i, j]);
                            istniejaceChromB.Add(Pop[i + 1, j]);
                        }

                        istniejaceChromA.Clear();
                        istniejaceChromB.Clear();

                    }
                }
                else
                {

                    for (int j = 0; j < PopTmp.GetLength(1); j++)
                    {

                        Pop[i, j] = PopTmp[i, j];
                        Pop[i + 1, j] = PopTmp[i + 1, j];

                    }

                }


            }
            }
         

        static void mutacja(int[,]Pop)
        {

            
             for (int i = 0; i < Pop.GetLength(0); i++)
             {
                 Random rp = new Random(i);
                 //mutujemy
                 if (rp.Next(0, 100) <= 95)
                 {
                     int split1 = rp.Next(0, Pop.GetLength(1) - 1);
                     int split2;
                     do
                     {
                         split2 = rp.Next(0, Pop.GetLength(1) - 1);
                     } while (split1 == split2);
                     int pier = split1;
                     int dr = split2;
                     
                     int tmp = Pop[i, pier];
                     Pop[i, pier] = Pop[i, dr];
                     Pop[i, dr] = tmp;
                 }
             }

             //print pop
            
           
        }
        static void printBest(int[,]Pop)
        {
                for (int j = 0; j < Pop.GetLength(1); j++)
                {


                    Console.Write(Pop[3, j]+" ");
                }
                Console.WriteLine();
                

            
        }

        
  
    }
}
